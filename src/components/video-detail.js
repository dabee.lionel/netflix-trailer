import React from "react";

const VideoDetail = ({ title, description }) => {
  return (
    <div style={{ marginTop: "2em" }}>
      <h1>{title}</h1>
      <p>{description}</p>
    </div>
  );
};

export default VideoDetail;
